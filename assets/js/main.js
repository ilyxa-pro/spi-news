var __opts = { popup: {} }
__opts.popup.defaults = {
    tClose: 'Закрыть (Esc)',
    tLoading: 'Загрузка...',
    type: 'inline',
    fixedContentPos: true,
    fixedBgPos: true,
    image: {
        tError: 'Произошла ошибка при загрузке <a href="%url%">изображения</a>.'
    },
    ajax: {
        settings: null,
        cursor: 'isProcessed',
        tError: 'Ошибка при загрузке <a href="%url%">содержимого</a>'
    },
    callbacks: {
        beforeOpen: function() {
            $('body').addClass('withModalOpen');
        },
        afterClose: function() {
            $('body').removeClass('withModalOpen');
        },
    },
    verticalFit: true,

    closeBtnInside: true,
    preloader: true,
    removalDelay: 500,
    mainClass: 'mfp-slide-bottom'
};
__opts.popup.gallery = $.extend(true, {}, __opts.popup.defaults, {
    gallery: {
        enabled: true,
        tPrev: 'Предыдущее (&larr;)',
        tNext: 'Следующее (&rarr;)',
        tCounter: '%curr% из %total%',
        preload: [1, 2],
    },
    modal: false,
    type: 'image',
    closeBtnInside: false,
})
$.ajaxSetup({ cache: false });




$(function() {



    $(document).scroll(function() {
        var fixbannerH = $(".fixbanner").height();

        if ($(this).scrollTop() > fixbannerH) {
            $("body").addClass("scroll-top");
        } else $("body").removeClass("scroll-top");
    });

    $(".mobileBlock").click(function() {
        $("body").toggleClass("active-menu");
    });

    $(".city-name").click(function() {
        $(this).parents(".city").toggleClass("open");
    });

    $(document).mouseup(function(e) {
        var cityBox = $(".city-name");
        if (!cityBox.is(e.target) &&
            cityBox.has(e.target).length === 0) {
            $(".city").removeClass("open");
        }
    });
















































    //Главный слайдер
    $('.slideListBlock').each(function() {
        var owner = this;

        var thresholds = [1920, 1600, 1440, 1336, 1280, 1024, 960, 800, 720, 640, 320, 0];

        $('.image', this).each(function() {
            var owner = this;
            var img = new Image;
            img.onload = function() {
                for (i in thresholds) {
                    if (img.width >= thresholds[i]) {
                        owner.setAttribute('data-width-threshold', thresholds[i]);
                        break;
                    }
                }
            };
            img.src = this.src;
        })

        var html = [];
        $('.slideList .slide', this).each(function() {
            html.push("<a href='#' class='slide'></a>");
        })

        $(this).append($("<div class='navBlock navBlock-steps'><div class='blockWrapper'><a href='#' class='step step-prev'>Предыдущий</a><a href='#' class='step step-next'>Следующий</a></div></div><div class='navBlock navBlock-slides'><div class='blockWrapper'>" + html.join('') + '</div></div>'));

        this.__switch = function(idx) {
            if (typeof(idx) == 'undefined') {
                this.__current++;
            } else {
                if (idx == 'prev' || idx == 'next') {
                    this.__current += idx == 'prev' ? -1 : 1;
                } else {
                    this.__current = idx;
                }
            }

            if (this.__current < 0) {
                this.__current = $('.slideList .slide', owner).length;
            }
            if (this.__current >= $('.slideList .slide', owner).length) {
                this.__current = 0;
            }

            $('.slideList .slide', owner).removeClass('slide-active').eq(this.__current).addClass('slide-active');
            $('.navBlock-slides .slide', owner).removeClass('slide-active').eq(this.__current).addClass('slide-active');
        }
        this.__switch(0);

        $(this).mouseleave(function() {
            this.__interval = window.setInterval(function() {
                owner.__switch();
            }, ($(owner).data('slide-interval') || 5) * 1000);
        }).mouseenter(function() {
            if (this.__interval) { window.clearInterval(this.__interval); }
        }).mousemove(function() {
            if (this.__interval) { window.clearInterval(this.__interval); }
        })

        $('.navBlock-steps', this).on('click', '.step', function(e) {
            e.preventDefault();
            owner.__switch($(this).hasClass('step-prev') ? 'prev' : 'next');
            return false;
        });

        $('.navBlock-slides', this).on('click', '.slide', function(e) {
            e.preventDefault();
            owner.__switch($(this).index());
            return false;
        });
    });


    /*Мелкие слайдеры*/
    $('.frame-partners .partnersList, .frame-news .newsList, .frame-reviews .reviewsList').each(function() {
        $(this).wrap($("<div class='catalogScrollBlock'><div class='scrollWrapper'></div></div>"));
        var owner = this.parentNode.parentNode;
        console.log(owner);
        $(owner).prepend($("<div class='scrollNavBlock'><a href='#' class='step step-prev'></a><a href='#' class='step step-next'></a></div><div class='navBlock navBlock-slides'></div>"));
        var $list = $(this);
        var $outer = $list.parent();

        $('.item:first-child', $list).addClass('item-active');

        owner.__repos = function(auto) {
            if (!this.__offset) { this.__offset = 0; }


            var listWidth = $('.item:last-child', $list);
            listWidth = listWidth.outerWidth(true) + listWidth.position().left;

            var steps = [];
            steps.length = Math.ceil(listWidth / $outer.outerWidth());

            $('.navBlock-slides', owner).html('<a class="slide" href="#">' + steps.join('</a> <a class="slide" href="#">') + '</a>');





            if (auto == true) {
                var diff = owner.__offset / $outer.width();
                var diffCount = Math.floor(diff);

                owner.__offset = $outer.width() * Math.round(diff - diffCount);
            }

            $('.scrollNavBlock .step', owner).removeClass('step-inactive');
            $('.navBlock-slides .slide', owner).removeClass('slide-active');

            if ($outer.width() < listWidth) {
                $(owner).addClass('isOverflown');

                $('.scrollNavBlock .step', owner).removeClass('step-inactive');
                if (owner.__offset <= 0) {
                    owner.__offset = 0;
                    $('.scrollNavBlock .step-prev', owner).addClass('step-inactive');
                }
                if (owner.__offset + $outer.width() >= listWidth) {
                    owner.__offset = listWidth - $outer.width();
                    $('.scrollNavBlock .step-next', owner).addClass('step-inactive');
                }
            } else {
                $(owner).removeClass('isOverflown');
                this.__offset = 0;
                $('.scrollNavBlock .step', owner).addClass('step-inactive');
            }
            this.__offset = Math.round(this.__offset);
            var idx = Math.floor(this.__offset / $outer.width());

            console.log(idx, this.__offset, $outer.width(), this.__offset / $outer.width());

            $('.navBlock-slides .slide:eq(' + idx + ')', owner).addClass('slide-active');

            $list.css('left', -this.__offset);
        }
        owner.__repos(true);

        $(owner).on('click', '.navBlock-slides .slide', function() {
            var idx = $(this).index();
            owner.__offset = idx * $outer.width();
            owner.__repos(true);
            return false;


        })

        $(owner).on('click', '.scrollNavBlock .step', function() {
            var direction = $(this).hasClass('step-next') ? 1 : -1;
            //console.log(direction);
            if ($(owner).hasClass('imageGalleryBlock-compact')) {
                var idx = $('.item-active', $list).index() + direction;
                if (idx < 0) {
                    idx = $('.item', $list).length - 1;
                }
                if (idx > $('.item', $list).length - 1) {
                    idx = 0;
                }
                $('.item', $list).removeClass('item-active').eq(idx).find('>a').click();
            } else {
                owner.__offset = owner.__offset + direction * $outer.width();
            }
            owner.__repos();
            return false;
        })

        $('.item:first-child a', $list).click();



        $(window).resize(function() {

            $('.frame-partners .partnersList, .frame-news .newsList, .frame-reviews .reviewsList').each(function() {
                this.parentNode.parentNode.__repos(true);
            });
        });





        //Галерея



        $('.imageGalleryBlock').each(function() {
            var owner = this;
            $(this).prepend($("<div class='previewBlock'><div class='imageHolder'><div class='imageWrapper'><div class='box-slide-image'><img src='' class='image' /></div></div></div></div>"));
            $('.imageListBlock', this).append($("<div class='navBlock navBlock-steps'><a href='#' class='step step-prev'>Назад</a><a href='#' class='step step-next'>Вперёд</a></div>"));
            var $list = $('.imageList', this);
            var $outer = $list.parent();
            $('.item:first-child', $list).addClass('item-active');
            this.__repos = function(auto) {
                if (auto == true) {
                    owner.__offset = -($outer.width() / 2 - $('.item-active', $list).offset().left - $('.item-active', $list).width() / 2 + $list.offset().left);
                }
                if ($outer.width() < $list.width()) {
                    $('.navBlock .step', owner).removeClass('step-inactive');
                    if (owner.__offset < 0) {
                        owner.__offset = 0;
                        $('.navBlock .step-prev', owner).addClass('step-inactive');
                    }
                    if (owner.__offset + $outer.width() > $list.width()) {
                        owner.__offset = $list.width() - $outer.width();
                        $('.navBlock .step-next', owner).addClass('step-inactive');
                    }
                } else {
                    owner.__offset = 0;
                    $('.navBlock .step', owner).addClass('step-inactive');
                }
                $list.css('left', -this.__offset);
            }
            this.__repos(true);
            $(this).on('click', '.navBlock .step', function() {
                var direction = $(this).hasClass('step-next') ? 1 : -1;
                console.log(direction);
                if ($(owner).hasClass('imageGalleryBlock-compact')) {
                    var idx = $('.item-active', $list).index() + direction;
                    if (idx < 0) {
                        idx = $('.item', $list).length - 1;
                    }
                    if (idx > $('.item', $list).length - 1) {
                        idx = 0;
                    }
                    $('.item', $list).removeClass('item-active').eq(idx).find('>a').click();
                } else {
                    owner.__offset = owner.__offset + direction * $outer.width();
                }
                owner.__repos();
                return false;
            })
            $list.on('click', '.item a', function() {
                var url = this.href;
                $('.previewBlock', owner).addClass('previewBlock-loading');
                $('.previewBlock .image', owner).fadeOut(500, function() {
                    var img = new Image;
                    img.onload = function() {
                        $('.previewBlock', owner).removeClass('previewBlock-loading');
                        $('.previewBlock .image', owner).attr('src', this.src).fadeIn(500);
                    }
                    img.src = url;
                });
                $('.item', $list).removeClass('item-active');
                $(this.parentNode).addClass('item-active');
                owner.__repos(true);
                return false;
            })
            $('.item:first-child a', $list).click();
        });





        $('.imageGalleryBlock .previewBlock').on('click', '.image', function(e) {
            if ($.fn.magnificPopup) {
                var owner = $(this).parents('.imageGalleryBlock').children('.imageListBlock');
                if (!owner.__items) {
                    owner.__items = [];
                    $('.item a', owner).each(function() {
                        owner.__items.push({ src: this.href, type: 'image' });
                    })
                }
                $.magnificPopup.open($.extend(true, {}, __opts.popup.gallery, { items: owner.__items }), $(this.parentNode).index());
                return false;
            }
        });

    });



































































    $('html').removeClass('jsLoading').addClass('jsLoaded');
});